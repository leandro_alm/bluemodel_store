using System.Collections.Generic;
using BlueModas.Entities;

namespace BlueModas.Models
{
    public class CartViewModel
    {
        public List<TmpOrders> ListaCarrinho { get; set; }
    }
}