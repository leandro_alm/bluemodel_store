using System.Collections.Generic;
using BlueModas.Entities;

namespace BlueModas.Models
{
    public class CheckoutViewModel
    {
        public List<TmpOrders> ListaCarrinho { get; set; }
        public Orders Pedido { get; set; }
        public string Token { get; set; }
        public CheckoutViewModel(){
            this.Pedido = new Orders();
            ListaCarrinho= new List<TmpOrders>();
        }
    }
}