using System.Collections.Generic;
using BlueModas.Entities;

namespace BlueModas.Models
{
    public class ResumeViewModel
    {
        public string Token { get; set; }
        public Orders Pedido { get; set; }

        public ResumeViewModel(){
            Pedido = new Orders();
        }
    }
}