using BlueModas.Entities;
using Microsoft.EntityFrameworkCore;
using BlueModas.Helpers;

namespace BlueModas.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Products> Products { get; set; }
        public DbSet<TmpOrders> TmpOrders { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<OrderProducts> OrderProducts { get; set; }
    }
}