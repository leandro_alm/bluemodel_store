using System.Collections.Generic;

namespace BlueModas.Entities
{
    public class Orders
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public decimal PrecoTotal{get;set;}
        public List<OrderProducts> ListOrderProducts { get; set; }
    }
}