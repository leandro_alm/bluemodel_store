namespace BlueModas.Entities
{
    public class OrderProducts
    {
        public int Id{get;set;}
        public int OrdersId{get;set;}
        public int ProductsId{get;set;}
        public decimal Qtd{get;set;}
        public decimal Price{get;set;}

        public Products Products{get;set;}
        public Orders Orders{get;set;}
    }
}