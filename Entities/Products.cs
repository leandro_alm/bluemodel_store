using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;
using System.IO;

namespace BlueModas.Entities
{
    public class Products
    {
        public int Id { get;set;}
        public string Name { get;set;}
        public decimal Price { get;set;}
        public string Image{get;set;}
    }
}