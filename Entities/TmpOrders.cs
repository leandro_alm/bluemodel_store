using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BlueModas.Entities
{
    public class TmpOrders
    {
        public int Id { get; set; }
        [Required]
        public int ProductsId { get; set; }
        [Required]
        public string Token { get; set; }
        [Required]
        public int Qtd { get; set; }

        public Products Products { get; set; }
    }
}