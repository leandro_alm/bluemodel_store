using System;
using System.Drawing;
using System.IO;

namespace BlueModas.Helpers
{
    public static class ConvertImgTo64Base
    {
        public static string ConvertToBase64(string img_path, string extension)
        {
            string base64String = "";
            string path = "./wwwroot/images/";
            if (!string.IsNullOrWhiteSpace(img_path))
            {                
                using (Image image = Image.FromFile(System.IO.Path.Combine(path, $"{img_path}.{extension}")))
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        image.Save(m, image.RawFormat);
                        byte[] imageBytes = m.ToArray();

                        base64String = Convert.ToBase64String(imageBytes);
                        base64String=base64String.Insert(0, $"data:image/{extension};base64,");
                        return base64String;
                    }
                }
            }
            return base64String;
        }
    }
}