$('.product-qtd').on('change', function (data) {
    let href = $(this).data('update-href');
    $.ajax({
        url: href,
        data: { "id": $(this).data('id'), "token": auth_id, "quantity": $(this).val() },
        error: function (err) {
            showError(err.responseJSON);
        }
    });
});

$('.product-remove').on('click', function (event) {
    event.preventDefault();
    $.ajax({
        url: $(this).attr('href'),
        data: { "token": auth_id },
        success: function (data) {
            location.reload();
            return false;
        },
        error: function (err) {
            showError(err.responseJSON);
        }
    })
});

(function update() {
    if($('#checkout').length >0){
        let href = $('#checkout').attr('href');
        $('#checkout').attr('href', `${href.split('?')[0]}?token=${auth_id}`);
    }
}());
