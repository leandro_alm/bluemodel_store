﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
var auth_id=''; 
var telMask = ['(99) 9999-99999', '(99) 99999-9999'];

var authenticate = (function auth(){
    sessionStorage.SessionName = "BMAuth";
    auth_id = sessionStorage.getItem("auth_id");
    if(!auth_id){
        auth_id= create_UUID();
        sessionStorage.setItem("auth_id", auth_id);
    }
    let cart = $('#CartButton');
    let new_href = cart.attr('href');
    cart.attr('href', `${new_href}?token=${auth_id}`);
    return auth;
}());

function create_UUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}

function showError(message){
    $('#divError').html('');
    $('#divError').html(message);
    $('#divError').show();
}

function inputHandler(masks, max, event) {
    var c = event.target;
    var v = c.value.replace(/\D/g, '');
    var m = c.value.length > max ? 1 : 0;
    VMasker(c).unMask();
    VMasker(c).maskPattern(masks[m]);
    c.value = VMasker.toPattern(v, masks[m]);
}    
  