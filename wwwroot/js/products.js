function AddItemToCart(itemId){
    let href = $('#cartUrl').attr('href');
    $.ajax({
        url: href,
        data: { "productId": itemId, "token": auth_id },
        method: 'POST',
        success: function(data){
            //TODO: Mostrar na aba de carrinho um icone mostrando a qtd
        },
        error: function(err){
            showError(err.responseJSON);
        }
    });   
}
