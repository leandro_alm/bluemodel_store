using System.Linq;
using System.Threading.Tasks;
using BlueModas.Data;
using BlueModas.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BlueModas.Components
{
    public class ListProductsComponents : ViewComponent
    {
        readonly DataContext _context;
        public ListProductsComponents(DataContext context)
        {
            this._context = context;
        }
        public async Task<IViewComponentResult> InvokeAsync(string token)
        {
            ListProductsViewModel model = new ListProductsViewModel();
            model.ListaCarrinho = await _context.TmpOrders.Include(x => x.Products).Where(c => c.Token == token).ToListAsync();
            return View("~/Views/Cart/_ListProducts.cshtml", model);
        }

    }
}