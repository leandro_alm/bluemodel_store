﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BlueModas.Migrations
{
    public partial class alterTmpOrders_3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProductId",
                table: "TmpOrders",
                newName: "ProductsId");

            migrationBuilder.AddColumn<int>(
                name: "TmpOrdersId",
                table: "Products",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_TmpOrdersId",
                table: "Products",
                column: "TmpOrdersId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_TmpOrders_TmpOrdersId",
                table: "Products",
                column: "TmpOrdersId",
                principalTable: "TmpOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_TmpOrders_TmpOrdersId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_TmpOrdersId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "TmpOrdersId",
                table: "Products");

            migrationBuilder.RenameColumn(
                name: "ProductsId",
                table: "TmpOrders",
                newName: "ProductId");
        }
    }
}
