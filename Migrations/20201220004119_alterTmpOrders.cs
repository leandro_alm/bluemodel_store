﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BlueModas.Migrations
{
    public partial class alterTmpOrders : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProductsJson",
                table: "TmpOrders",
                newName: "Token");

            migrationBuilder.AddColumn<int>(
                name: "ProductId",
                table: "TmpOrders",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "TmpOrders");

            migrationBuilder.RenameColumn(
                name: "Token",
                table: "TmpOrders",
                newName: "ProductsJson");
        }
    }
}
