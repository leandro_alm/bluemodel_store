﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BlueModas.Models;
using BlueModas.Entities;
using Microsoft.EntityFrameworkCore;
using BlueModas.Data;
using Newtonsoft.Json;

namespace BlueModas.Controllers
{
    public class HomeController : Controller
    {
        private DataContext _context;
        public HomeController(DataContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            HomeViewModel model = new HomeViewModel();
            model.listaProdutos = _context.Products.ToList();
            return View(model);
        }

        [HttpPost]
        public JsonResult AddToCart(int productId, string token)
        {
            try
            {
                if (productId != 0)
                {
                    if (this._context.TmpOrders.Any(c => c.ProductsId == productId && c.Token == token))
                    {
                        this._context.TmpOrders.FirstOrDefault(c => c.ProductsId == productId && c.Token == token).Qtd++;
                    }
                    else
                    {
                        TmpOrders tmpOrder = new TmpOrders() { ProductsId = productId, Token = token, Qtd = 1 };
                        this._context.Add(tmpOrder);
                    }
                    this._context.SaveChanges();
                }
            }
            catch (Exception)
            {
                Response.StatusCode = 500;
                return Json("Ocorreu um erro ao adicionar o produto no carrinho");
            }
            return Json("OK");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
