using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using BlueModas.Data;
using BlueModas.Entities;
using BlueModas.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BlueModas.Controllers
{
    public class CartController : Controller
    {
        private DataContext _context;
        public CartController(DataContext context)
        {
            _context = context;
        }

        public IActionResult Index(string token)
        {
            CartViewModel model = new CartViewModel();
            model.ListaCarrinho = model.ListaCarrinho = LoadCarrinho(token);
            return View(model);
        }

        public JsonResult UpdateOrderQtd(int id, string token, int quantity)
        {
            try
            {
                if (quantity > 0)
                {
                    try
                    {
                        this._context.TmpOrders.FirstOrDefault(c => c.Id == id && c.Token == token).Qtd = quantity;
                    }
                    catch { }
                    this._context.SaveChanges();
                }
            }
            catch (Exception)
            {
                Response.StatusCode = 500;
                return Json("Ocorreu um erro ao atualizar o carrinho");
            }
            return Json("OK");
        }

        public JsonResult RemoveFromCart(int id, string token)
        {
            try
            {
                this._context.TmpOrders.Remove(this._context.TmpOrders.FirstOrDefault(c => c.Id == id && c.Token == token));
                this._context.SaveChanges();
            }
            catch (Exception)
            {
                Response.StatusCode = 500;
                return Json("Ocorreu um erro ao remover o item do carrinho");
            }
            return Json("OK");
        }

        public ActionResult Checkout(string token)
        {
            CheckoutViewModel model = new CheckoutViewModel();
            model.Token = token;
            model.ListaCarrinho = LoadCarrinho(token);
            return View("Checkout", model);
        }
        private List<TmpOrders> LoadCarrinho(string token){
            return this._context.TmpOrders.Include(x => x.Products).Where(c => c.Token == token).ToList();
        }

        [HttpPost]
        public ActionResult Checkout(CheckoutViewModel model)
        {
            // ResumeViewModel resumeModel =new ResumeViewModel();
            try
            {
                this.validateOrder(model);
                if (ModelState.IsValid)
                {
                    model.Token = model.Token;
                    model.Pedido.Telefone = Regex.Replace(model.Pedido.Telefone, @"[^\d]", "").ToString();
                    this._context.Add(model.Pedido);
                    this._context.SaveChanges();
                    model.Pedido.ListOrderProducts = new List<OrderProducts>();
                    foreach (var item in this._context.TmpOrders.Include(x => x.Products).Where(c => c.Token == model.Token))
                    {
                        model.Pedido.ListOrderProducts.Add(new OrderProducts()
                        {
                            OrdersId = model.Pedido.Id,
                            Price = item.Products.Price,
                            ProductsId = item.ProductsId,
                            Qtd = item.Qtd,
                        });
                    }
                    model.Pedido.PrecoTotal = model.Pedido.ListOrderProducts.Sum(c => c.Price * c.Qtd);
                    this._context.Update(model.Pedido);
                    this._context.SaveChanges();
                    return RedirectToAction("FinalizarPedido", new { pedidoId = model.Pedido.Id, token = model.Token });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Ocorreu um erro ao finalizar o pedido");
            }
            model.ListaCarrinho = LoadCarrinho(model.Token);
            return View(model);
        }
        private void validateOrder(CheckoutViewModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Pedido.Nome))
            {
                // throw new Exception("Informe o nome.");
                ModelState.AddModelError("Pedido.Nome", "Informe o nome");
            }
            if (string.IsNullOrWhiteSpace(model.Pedido.Telefone))
            {
                // throw new Exception("Informe o telefone.");
                ModelState.AddModelError("Pedido.Telefone", "Informe o telefone");
            }
            if (string.IsNullOrWhiteSpace(model.Pedido.Email))
            {
                // throw new Exception("Informe o email.");
                ModelState.AddModelError("Pedido.Email", "Informe o email");
            }
        }
        public ActionResult FinalizarPedido(int pedidoId, string token)
        {
            ResumeViewModel model = new ResumeViewModel();
            model.Token = token;
            model.Pedido = this._context.Orders.FirstOrDefault(c => c.Id == pedidoId);
            return View(model);
        }
    }
}