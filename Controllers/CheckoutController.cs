using System;
using System.Linq;
using BlueModas.Data;
using BlueModas.Entities;
using BlueModas.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BlueModas.Controllers
{
    public class CheckoutController : Controller
    {
        private DataContext _context;
        public CheckoutController(DataContext context)
        {
            _context = context;
        }

        public IActionResult Index(string token)
        {
            CartViewModel model = new CartViewModel();
            model.ListaCarrinho = _context.TmpOrders.Include(x => x.Products).Where(c => c.Token == token).ToList();
            return View(model);
        }

        public JsonResult UpdateOrderQtd(int id, string token, int quantity)
        {
            try
            {
                if (quantity > 0)
                {
                    try
                    {
                        this._context.TmpOrders.FirstOrDefault(c => c.Id == id && c.Token == token).Qtd = quantity;
                    }
                    catch { }
                    this._context.SaveChanges();
                }
            }
            catch (Exception)
            {
                Response.StatusCode = 500;
                return Json("Ocorreu um erro ao atualizar o carrinho");
            }
            return Json("OK");
        }

        public JsonResult RemoveFromCart(int id, string token)
        {
            try
            {
                this._context.TmpOrders.Remove(this._context.TmpOrders.FirstOrDefault(c => c.Id == id && c.Token == token));
                this._context.SaveChanges();
            }
            catch (Exception)
            {
                Response.StatusCode = 500;
                return Json("Ocorreu um erro ao remover o item do carrinho");
            }
            return Json("OK");
        }
    }
}